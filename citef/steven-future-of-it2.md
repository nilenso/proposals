# Business is Technology #

It is the paradox of the present generation of businesses that they are each, inherently, technology businesses. If every business is a "technology business", none of them are.

This truth stems from the fact that technology is adjacent to everything. Every legacy business will be automated and every piece of automation will feed on itself, spiralling the previous incarnations out of existence in shorter and shorter intervals. If the past two centuries of business have been born of "co-operation through shared imagination" [1], a foundationally co-operative model seems necessary to anticipate which entities might survive the next period of evolution.

We will discuss the microcosm of technology as seen through the lens of designers and developers at nilenso: Cellular and biological models for self-healing systems applied to distributed and autonomous organizations, the distribution of software viruses and viral licensing as models for distributing information, IoT (née ubiquitous computing) as a template for ubiquitous businesses and increasingly dissolving corporate granularity.

As the software industry has embraced Design Patterns and "habitability" [2], in the original sense of those terms, so must the enterprise. Employers which do not recognize the inherent value of their employees are doomed to create rigid business architectures too heavy and too rough to float or move in the evermore fluent global economy.

1. Yuval Noah Harari: http://www.ynharari.com/book/sapiens/
2. Christopher Alexander: https://en.wikipedia.org/wiki/The_Timeless_Way_of_Building