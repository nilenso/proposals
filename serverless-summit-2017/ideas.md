
lambda calculus vs. lambda service

https://blog.symphonia.io/defining-serverless-part-1-704d72bc8a32
https://martinfowler.com/articles/serverless.html


Serverless is the New Rails:
For many, auto-provisioning / auto-scaling single-instance execution environments will be too slow.
- excuse myself for being the old curmudgeon (C++ manual mem management, detractors of immutable data structures, etc.)
- DUPLO / PHP's `email` function

Serverless is still a leaky abstraction(s):
The developer now has zero control over the network stack, the database configuration, allocation of resources like threads, etc. In many cases, this is fine. In any sort of low-latency or high-volume services, this won't fly.
- spectrum: finance vs. google-scale vs. web-scale vs. enterprise

Vendor lock-in in sheep's clothing:
- how to migrate away from AWS Lambda?
- standards?

Why we can't outsource EP:
- 10ms 99.9th percentile won't work with optimizely, much less a Serverless arch
- how could we slap together server-side lambdas to collectively construct EP?

FaaS has all the problems of Heroku, multiplied:
- still no control over disk
- now no control over configuration, VM/JIT/hotspot, warm-up, etc.
- opposite of Mechanical Sympathy

How to deployment?
- deploying an AWS Lambda function is a script attached to deploy infra
- configuring the API Gateway is moving deployment activities to configuration mutation

"What is Serverless?"
- p17: "In short, we've reduced the labor cost of making the game, as well as the risk and resource costs of running it."


- plugins
- geojson
- interface (ui)
- ok w 2 people
- can cover travel
- DC can probably pay (tax)
- monthly billing better
- inception? - do offline
- time: august

todo:
- SoW
- deepa/abhik -- questions, meet?, commercials
