
co-op
transparent
collective
open source / data
menstrual leave policy
quiet, safe, comfortable - habitability
music, art
good tools
library
goban, board games
food
health
fair/transparent policies, salaries
future-focused work
tech: clojure/script, haskell/purescript -- fashion vs. progress

--

PROGRESS / EVOLUTION / THE REMIX

Baby steps. Your tomorrow is nearly known to you. Clarity in November is gently dulled. Two-thousand-nineteen contains mysteries for you but a steady advancement for our species. Your daughter's computer will scare you. It's okay.

We balk at nations which remain undemocratic. Democracy is not a Universal Good but it is the best system of governance we know, thus far. We accept lesser systems in the Enterprise. Nilenso does not. We are an employee-owned-and-operated Co-operative, legally and operationally. Our constitution is open source.

We believe in open source. We strive to open every repository, document, and data store we can because steady progress toward a healthier economy built on open knowledge seems obvious. We believe a company itself should be open and transparent. Our salaries and accounting are public to all employees. What is there to hide?

Our office is quiet, safe, comfortable. It is an old house. Warmly lit, filled with quality tools, musical instruments, art, healthy food, composters -- both leaf and bokashi, board games, a large library, warm showers, bicycle parking, and a plug for your electric vehicle. We have a no-questions-asked Menstrual Leave Policy because every organization should. Gabriel borrowed Alexander's notions of "habitability" and we feel the idea applies to our physical homes and virtual homes in equal proportion. This can be done without becoming self-indulgent or self-righteous. But we still tread carefully.

We write most of our high-level code in Clojure/ClojureScript and Haskell/PureScript because the arc of Computer Science isn't a fashion war. A good process and an honest, consistent, self-disciplined process... appear indistinguishable. _Il ne faut jamais mélanger les torchons et les serviettes._ The future of technology is not To-do Lists or To-do List Frameworks or To-do List Microservice Enterprise Integration Event Streams. We might build one along the way but it will never be the goal.

If the future of business is the future of technology then we would encourage you to borrow as many of these ideas as you see fit. Naturally, we take credit for none of them. Every idea is a borrowed idea: every new idea, a remix.

We would like to thank the Clojure Contributors for releasing one of our all-time favourite remixes. We wouldn't be here without it.

--
